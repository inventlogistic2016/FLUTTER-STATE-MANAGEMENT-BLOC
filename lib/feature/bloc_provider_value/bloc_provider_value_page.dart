import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc_provider_or_dependeuncy_injection/controller/controller_bloc_provider.dart';
import '../bloc_provider_or_dependeuncy_injection/widget/counter_data_builder.dart';
import 'other/other_page.dart';

class BlocProviderValuePage extends StatelessWidget {
  const BlocProviderValuePage({super.key});

  @override
  Widget build(BuildContext context) {
    ControlBlocProvider controlBlocProvider =
        BlocProvider.of<ControlBlocProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: const Text("Bloc Provider value"),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          //Navigator.of(context).push(MaterialPageRoute(builder: (context) => const OtherPage()));

          //todo ini untuk BlocProvider.value, ini yang disarankan
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => BlocProvider.value(
                value: controlBlocProvider,
                child: const OtherPage(),
              ),
            ),
          );
        },
        child: const Icon(Icons.arrow_forward),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          //todo bloc listener
          const Center(
            child: counterDataBuilder(),
          ),
          const SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              IconButton(
                onPressed: () {
                  controlBlocProvider.removeData();
                },
                icon: const Icon(Icons.remove),
              ),
              IconButton(
                onPressed: () {
                  controlBlocProvider.addData();
                },
                icon: const Icon(Icons.add),
              ),
            ],
          ),
        ],
      )),
    );
  }
}
