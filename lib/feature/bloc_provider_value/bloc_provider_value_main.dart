import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state_mangement_bloc/feature/bloc_provider_or_dependeuncy_injection/controller/controller_bloc_provider.dart';
import 'package:state_mangement_bloc/feature/bloc_provider_value/bloc_provider_value_page.dart';

class BlocProviderValueMain extends StatelessWidget {
  const BlocProviderValueMain({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BlocProvider(
        create: (context)=> ControlBlocProvider(),
        child: const BlocProviderValuePage()
      ),
    );

  }
}

