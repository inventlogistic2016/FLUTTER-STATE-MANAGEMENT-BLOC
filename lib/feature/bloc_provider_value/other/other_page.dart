import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state_mangement_bloc/feature/bloc_provider_or_dependeuncy_injection/controller/controller_bloc_provider.dart';


class OtherPage extends StatelessWidget {
  const OtherPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ControlBlocProvider controlBlocProvider =
    context.read<ControlBlocProvider>();
    return Scaffold(
      appBar: AppBar(
        title: const Text("Other Page"),
      ),
      body: Center(
        child: BlocBuilder<ControlBlocProvider, int>(
          bloc: controlBlocProvider,
          builder: (context, state) {
            return Text(
              "$state",
              style: const TextStyle(fontSize: 50),
            );
          },
        ),
      ),
    );
  }
}
