import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state_mangement_bloc/feature/bloc_selector/bloc_selector_page.dart';
import 'package:state_mangement_bloc/feature/bloc_selector/controller/UserBloc.dart';

class BlocSelectorMain extends StatelessWidget {
  const BlocSelectorMain({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BlocProvider(
        create: (context)=>UserBloc(),
        child: const BlocSelectorPage(),
      ),
    );
  }
}
