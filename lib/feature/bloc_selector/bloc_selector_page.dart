import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state_mangement_bloc/feature/bloc_selector/controller/UserBloc.dart';

import 'builder_selector/input_nama_builder.dart';
import 'builder_selector/input_umur_builder.dart';
import 'builder_selector/name_builder.dart';
import 'builder_selector/umur_builder.dart';



class BlocSelectorPage extends StatelessWidget {
  const BlocSelectorPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    UserBloc userBloc = context.read<UserBloc>();
    return Scaffold(
      appBar: AppBar(
        title: const Text("Bloc Selector"),
      ),
      body: ListView(
        padding: const EdgeInsets.all(20),
        children: const [
          NameBuilder(),
          UmurBuilder(),
          SizedBox(
            height: 20,
          ),
          InputNamaBuilder(),
          InputUmurBuilder()
        ],
      ),
    );
  }
}
