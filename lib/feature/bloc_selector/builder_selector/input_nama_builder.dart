import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state_mangement_bloc/feature/bloc_selector/controller/UserBloc.dart';

class InputNamaBuilder extends StatelessWidget {
  const InputNamaBuilder({super.key});

  @override
  Widget build(BuildContext context) {
    print("input nama");

    UserBloc userBloc = context.read<UserBloc>();
    return BlocBuilder<UserBloc, Map<String, dynamic>>(
      bloc: userBloc,
      builder: (context, state) {
        return  TextField(
          onChanged: (value)=> userBloc.changeName(value),
          decoration: const InputDecoration(border: OutlineInputBorder()),
        );

      },
    );
  }
}
