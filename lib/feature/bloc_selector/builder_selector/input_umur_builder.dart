import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state_mangement_bloc/feature/bloc_selector/controller/UserBloc.dart';

class InputUmurBuilder extends StatelessWidget {
  const InputUmurBuilder({super.key});

  @override
  Widget build(BuildContext context) {
    print("input umur");

    UserBloc userBloc =  context.read<UserBloc>();
    return BlocBuilder<UserBloc, Map<String,dynamic>>(
        bloc:userBloc,
        builder: (context,state){
          return   Row(
            children: [
              IconButton(
                  onPressed: () {
                    userBloc.changeAge(userBloc.state["age"]-1);
                  },
                  icon: const Icon(Icons.remove)),
              IconButton(
                  onPressed: () {
                    userBloc.changeAge(userBloc.state["age"]+1);
                  },
                  icon: const Icon(Icons.add)),
            ],
          );
        },
    );
  }
}
