import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state_mangement_bloc/feature/bloc_selector/controller/UserBloc.dart';

class NameBuilder extends StatelessWidget {
  const NameBuilder({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("NAMA");
    return BlocSelector<UserBloc, Map<String, dynamic>, String>(
      selector: (state) => state["name"],
      builder: (context, state) {
        return Text("NAMA : $state");
      },
    );
  }
}
