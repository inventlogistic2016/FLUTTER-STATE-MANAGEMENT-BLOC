import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state_mangement_bloc/feature/bloc_selector/controller/UserBloc.dart';

class UmurBuilder extends StatelessWidget {
  const UmurBuilder({super.key});

  @override
  Widget build(BuildContext context) {
    print("UMUR");
    return BlocSelector<UserBloc, Map<String, dynamic>, int>(
      selector: (state) => state["age"],
      builder: (context, state) {
        return Text("UMUR : $state");
      },
    );
  }
}
