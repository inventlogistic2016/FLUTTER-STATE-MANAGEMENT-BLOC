import 'package:flutter/material.dart';
import 'package:state_mangement_bloc/feature/bloc_builder/bloc_builder_page.dart';
import 'package:state_mangement_bloc/feature/bloc_consumer/bloc_consumer_page.dart';
import 'package:state_mangement_bloc/feature/bloc_listener/bloc_listener_page.dart';
import 'package:state_mangement_bloc/feature/cubit/cubit_page.dart';
import 'package:state_mangement_bloc/feature/observer_cubit/observer_cubit_page.dart';
import 'package:state_mangement_bloc/feature/stream/stream_page.dart';
import 'package:state_mangement_bloc/menu_item.dart';

class MenuFetchRemote extends StatelessWidget {
  MenuFetchRemote({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.grey[300],
          elevation: 0,
          toolbarHeight: 80,
          title: const Text(
            "FETCH REMOTE",
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15),
        child: ListView(
          children: [

          ],
        ),
      ),
    );
  }
}
