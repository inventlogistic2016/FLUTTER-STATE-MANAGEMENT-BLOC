import 'package:flutter/material.dart';
import 'package:state_mangement_bloc/feature/bloc_builder/bloc_builder_page.dart';
import 'package:state_mangement_bloc/feature/bloc_consumer/bloc_consumer_page.dart';
import 'package:state_mangement_bloc/feature/bloc_listener/bloc_listener_page.dart';
import 'package:state_mangement_bloc/feature/bloc_provider_or_dependeuncy_injection/bloc_provider_main.dart';
import 'package:state_mangement_bloc/feature/bloc_provider_value/bloc_provider_value_main.dart';
import 'package:state_mangement_bloc/feature/cubit/cubit_page.dart';
import 'package:state_mangement_bloc/feature/observer_cubit/observer_cubit_page.dart';
import 'package:state_mangement_bloc/feature/stream/stream_page.dart';
import 'package:state_mangement_bloc/menu_item.dart';

import '../bloc_access/menu_bloc_access.dart';
import '../bloc_multi_provider/app_multi_provider_main.dart';
import '../bloc_selector/bloc_selector_main.dart';
import '../bloc_selector/bloc_selector_page.dart';

class MenuBloc extends StatelessWidget {
   MenuBloc({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.grey[300],
          elevation: 0,
          toolbarHeight: 80,
          title: const Text(
            "STATE MANAGEMENT BLOC",
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15),
        child: ListView(
          children: [
            MenuItem(text: "1.STREAM", pageBuilder: () => const StreamPage()),
            MenuItem(text: "2.CUBIT", pageBuilder: () => CubitPage()),
            MenuItem(text: "3.CUBIT OBSERVER", pageBuilder: () => ObserverCubitPage()),
            MenuItem(text: "4.BLOC BUILDER", pageBuilder: () => BlocBuilderPage()),
            MenuItem(text: "5.BLOC LISTENER", pageBuilder: () => BlocListenerPage()),
            MenuItem(text: "6.BLOC CONSUMER", pageBuilder: () => BlocConsumerPage()),
            MenuItem(text: "7.BLOC DEPENDENCY INJECTION", pageBuilder: () =>  const BlocProviderMain()),
            MenuItem(text: "8.BLOC PROVIDER VALUE", pageBuilder: () =>  const BlocProviderValueMain()),
            MenuItem(text: "9.BLOC ACCESS", pageBuilder: () =>   MenuBlocAccess()),
            MenuItem(text: "10.BLOC MULTI PROVIDER", pageBuilder: () =>  AppMultiProviderMain()),
            MenuItem(text: "11.BLOC SELECTOR", pageBuilder: () =>  const BlocSelectorMain()),


          ],
        ),
      ),
    );
  }
}
