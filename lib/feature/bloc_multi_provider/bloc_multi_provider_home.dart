import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state_mangement_bloc/feature/bloc_multi_provider/controller/bloc_thema.dart';

import 'bloc_multi_provider_page.dart';

class BlocMultiProviderHome extends StatelessWidget {
  const BlocMultiProviderHome({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ThemeBloc, bool>(
      bloc: context.read<ThemeBloc>(),
      builder: (context, state) {
        return MaterialApp(
          theme: state == true? ThemeData.light():ThemeData.dark(),
          home: const BlocMultiProviderPage(),
        );
      },
    );
  }
}
