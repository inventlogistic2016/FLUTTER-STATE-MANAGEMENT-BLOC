import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state_mangement_bloc/feature/bloc_multi_provider/controller/bloc_thema.dart';

import '../bloc_multi_listener/data_bulider_multi_listener.dart';
import '../bloc_provider_or_dependeuncy_injection/controller/controller_bloc_provider.dart';
import '../bloc_provider_or_dependeuncy_injection/widget/counter_data_builder.dart';

class BlocMultiProviderPage extends StatelessWidget {
  const BlocMultiProviderPage({super.key});

  @override
  Widget build(BuildContext context) {

    ThemeBloc themeBloc = context.read<ThemeBloc>();
    ControlBlocProvider controlBlocProvider = context.read<ControlBlocProvider>();

    return Scaffold(
      appBar: AppBar(
        title: const Text("BLOC MULTI PROVIDER AND LISTENER"),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          themeBloc.changeTheme();
        },
        child: const Icon(Icons.arrow_forward),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          //todo bloc listener
          const Center(
            child: DataBuilderMultiListener(),
          ),
          const SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              IconButton(
                onPressed: () {
                  controlBlocProvider.removeData();
                },
                icon: const Icon(Icons.remove),
              ),
              IconButton(
                onPressed: () {
                  controlBlocProvider.addData();
                },
                icon: const Icon(Icons.add),
              ),
            ],
          ),
        ],
      )),
    );
  }
}
