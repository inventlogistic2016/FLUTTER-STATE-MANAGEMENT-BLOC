import 'package:bloc/bloc.dart';

class ThemeBloc extends Cubit<bool>{

  //todo treu -> light flase -> dark
  ThemeBloc () : super(true);

  void changeTheme() => emit(!state);
}