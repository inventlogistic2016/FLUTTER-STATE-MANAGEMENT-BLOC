import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state_mangement_bloc/feature/bloc_multi_provider/controller/bloc_thema.dart';

import '../../bloc_access/generad_route-acces/404/404.dart';
import '../../bloc_provider_or_dependeuncy_injection/controller/controller_bloc_provider.dart';
import '../bloc_multi_provider_home.dart';

class AppRoute {


  Route onRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case "/":
        return MaterialPageRoute(
          builder: (context) => MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) => ControlBlocProvider(),
              ),
              BlocProvider(
                create: (context) => ThemeBloc(),
              ),
            ],
            child: const  BlocMultiProviderHome(),
          ),
        );

      default:
        return MaterialPageRoute(
          builder: (context) => const NotFoundPage(),
        );
    }
  }
}
