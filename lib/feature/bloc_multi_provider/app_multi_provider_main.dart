import 'package:flutter/material.dart';
import 'package:state_mangement_bloc/feature/bloc_multi_provider/router/app_router.dart';


class AppMultiProviderMain extends StatelessWidget {
    AppMultiProviderMain({super.key});
   final router = AppRoute();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      onGenerateRoute: router.onRoute,
    );
  }
}

