import 'package:flutter/material.dart';
import 'package:state_mangement_bloc/menu_item.dart';

import 'google_maps/google_maps_page.dart';

class MenuGoogleServices extends StatelessWidget {
  MenuGoogleServices({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey[300],
        elevation: 0,
        toolbarHeight: 80,
        title: const Text(
          "GOOGLE SERVICES",
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15),
        child: ListView(
          children: [
            MenuItem(text: "GOOGLE MAPS", pageBuilder: () => const GoogleMapsPage()),
          ],
        ),
      ),
    );
  }
}
