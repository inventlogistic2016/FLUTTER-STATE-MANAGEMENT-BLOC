import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:state_mangement_bloc/feature/menu_google_services/google_maps/helper/marker_generator.dart';
import 'package:state_mangement_bloc/feature/menu_google_services/google_maps/marker.dart';

import 'data/locations.dart' as locations;

class GoogleMapsPage extends StatefulWidget {
  const GoogleMapsPage({super.key});

  @override
  State<GoogleMapsPage> createState() => _GoogleMapsPageState();
}

class _GoogleMapsPageState extends State<GoogleMapsPage> {
  List<Marker> markers = [];

  final Completer<GoogleMapController> _controller =
      Completer<GoogleMapController>();

  final Map<MarkerId, Marker> _markers = {};
  final Map<PolylineId, Polyline> _polylines = {};

  List<LatLng> polylineCoordinates = [];
  final double _originLatitude = -6.176987, _originLongitude = 106.788586;
  final double _destLatitude = -6.1786083, _destLongitude = 106.8280924;

  PolylinePoints polylinePoints = PolylinePoints();
  String googleAPiKey = "AIzaSyAXzlH4FLbrsSHQzMEs-uVmRJH0ZjtbhcI";

  static const CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  static const CameraPosition _kIndo = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(-6.200000, 106.816666),
      tilt: 59.440717697143555,
      zoom: 5);

  late List<Uint8List> bitmaps;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GoogleMap(
        mapType: MapType.normal,
        initialCameraPosition: _kGooglePlex,
        onMapCreated: _onMapCreated,
        markers: _markers.values.toSet(),
        //polylines: _polylines.values.toSet(), // bayar setiap menggunakan Routes API
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: _goToTheLake,
        label: const Text('To Indonesia!'),
        icon: const Icon(Icons.directions),
      ),
    );
  }

  Future<void> _onMapCreated(GoogleMapController controller) async {
    final googleOffices = await locations.getGoogleOffices();
    _controller.complete(controller);

    setState(() {
      _markers.clear();

      // GENERATE MARKER FIRST
      MarkerGenerator(markerWidgets(googleOffices.offices), (bitmaps) {
        this.bitmaps = bitmaps;

        var index = 0;
        for (final office in googleOffices.offices) {
          _addMarker(LatLng(office.lat, office.lng), office.id, office.name,
              office.address, office.image, index);
          index++;
        }
      }).generate(context);
    });
    // _initPoly(); // bayar setiap menggunakan Routes API
  }

  Future<void> _goToTheLake() async {
    final GoogleMapController controller = await _controller.future;
    await controller.animateCamera(CameraUpdate.newCameraPosition(_kIndo));
  }

  _addMarker(
      LatLng position, String id, String name, String address, String url, int index) {

    MarkerId markerId = MarkerId(id);

    _markers[markerId] = Marker(
      markerId: MarkerId(id),
      position: position,
      icon: index != 010 ? BitmapDescriptor.fromBytes(bitmaps[index])
          : BitmapDescriptor.defaultMarker,
      infoWindow: InfoWindow(
        title: name,
        snippet: address,
      ),
    );

    setState(() { });
  }

  _initPoly() {
    _addMarker(LatLng(_originLatitude, _originLongitude), "centralpark",
        "Central Park", "Jl. Lorem Ipsum", "", 010);

    _addMarker(LatLng(_destLatitude, _destLongitude), "monas", "Monas",
        "Jl. Lorem Ipsum 2", "", 010);

    _getPolyline();
  }

  _addPolyLine() {
    PolylineId id = const PolylineId("poly");
    Polyline polyline = Polyline(
        polylineId: id, color: Colors.green, points: polylineCoordinates);
    _polylines[id] = polyline;
    setState(() {});
  }

  _getPolyline() async {
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
        googleAPiKey,
        PointLatLng(_originLatitude, _originLongitude),
        PointLatLng(_destLatitude, _destLongitude),
        travelMode: TravelMode.driving,
        wayPoints: [PolylineWayPoint(location: "Central Park ke Monas")]);

    if (result.points.isNotEmpty) {
      for (var point in result.points) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      }
    }
    _addPolyLine();
  }

  List<Widget> markerWidgets(List<locations.Office> offices) {
    return offices.map((c) => MarkerMaps(name: c.name)).toList();
  }

}
