import 'package:flutter/material.dart';

import 'my_router/my_route.dart';

class GeneratedRouteAccessMain extends StatelessWidget {
    GeneratedRouteAccessMain({super.key});
   final router = MyRoute();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      onGenerateRoute: router.onRoute,
    );
  }
}

