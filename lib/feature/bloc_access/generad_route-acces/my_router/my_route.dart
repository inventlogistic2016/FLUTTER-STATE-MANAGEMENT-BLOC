import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../bloc_provider_or_dependeuncy_injection/controller/controller_bloc_provider.dart';
import '../../../bloc_provider_value/other/other_page.dart';
import '../../named_route_access/named_route_access_page.dart';
import '../404/404.dart';

class MyRoute {
  final ControlBlocProvider controlBlocProvider = ControlBlocProvider();

  Route onRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case "/":
        return MaterialPageRoute(
          builder: (context) => BlocProvider.value(
            value: controlBlocProvider,
            child: const NamedRouteAccessPage(),
          ),
        );
      case "/other":
        return MaterialPageRoute(
          builder: (context) => BlocProvider.value(
            value: controlBlocProvider,
            child: const OtherPage(),
          ),
        );
      default:
        return MaterialPageRoute(
          builder: (context) => const NotFoundPage(),
        );
    }
  }
}
