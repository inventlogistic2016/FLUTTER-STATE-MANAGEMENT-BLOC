import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state_mangement_bloc/feature/bloc_provider_or_dependeuncy_injection/controller/controller_bloc_provider.dart';
import 'package:state_mangement_bloc/feature/bloc_provider_value/bloc_provider_value_page.dart';

import '../../bloc_provider_value/other/other_page.dart';
import 'named_route_access_page.dart';

class NameRouteAccessMain extends StatelessWidget {
   NameRouteAccessMain({super.key});

  final ControlBlocProvider controlBlocProvider = ControlBlocProvider();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      initialRoute: "/",
      routes: {
        "/": (context) =>
            BlocProvider.value(
              value: controlBlocProvider,
              child: const NamedRouteAccessPage(),
            ),
        "/other": (context) =>
            BlocProvider.value(
              value: controlBlocProvider,
              child: const OtherPage(),
            ),
      },
    );
  }
}

