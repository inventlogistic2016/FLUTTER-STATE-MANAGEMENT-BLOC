import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state_mangement_bloc/feature/bloc_multi_provider/controller/bloc_thema.dart';

import '../bloc_listener/controller/controller_bloc_listener.dart';
import '../bloc_provider_or_dependeuncy_injection/controller/controller_bloc_provider.dart';

class DataBuilderMultiListener extends StatelessWidget {
  const DataBuilderMultiListener({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
        listeners: [
          //todo COUNTER
          BlocListener<ControlBlocProvider, int>(
            listener: (context, state) {
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  duration: Duration(seconds: 1),
                  content: Text("ini data genap"),
                ),
              );
            },
            listenWhen: (previous, current) {
              //todo ini kondisi menampilkan data genap
              if (current % 2 == 0) {
                return true;
              } else {
                return false;
              }
            },
          ),

          //TODO THEME DRAK
          BlocListener<ThemeBloc, bool>(
            listener: (context, state) {
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  duration: Duration(seconds: 1),
                  content: Text("THEMA GELAP AKTIF"),
                ),
              );
            },
            listenWhen: (previous, current) {
              //todo ini kondisi menampilkan theme
              if (current == false) {
                return true;
              } else {
                return false;
              }
            },
          ),
        ],
        child: BlocBuilder<ControlBlocProvider, int>(
            bloc: context.read<ControlBlocProvider>(),
            builder: (context, state) {
              return Text(
                "$state",
                style: const TextStyle(fontSize: 50),
              );
            }));
  }
}
