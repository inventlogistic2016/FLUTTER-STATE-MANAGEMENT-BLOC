import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state_mangement_bloc/feature/bloc_provider_or_dependeuncy_injection/controller/controller_bloc_provider.dart';

import 'widget/counter_data_builder.dart';

class BlocProviderPage extends StatelessWidget {

   const BlocProviderPage({super.key});

  @override
  Widget build(BuildContext context) {


    ControlBlocProvider controlBlocProvider = BlocProvider.of<ControlBlocProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: const Text("Bloc Provider or dependency injection"),
      ),

      body: Center(
          child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            //todo bloc listener
            const Center(
              child: counterDataBuilder(),
            ),
            const SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IconButton(
                  onPressed: () {
                    controlBlocProvider.removeData();
                  },
                  icon: const Icon(Icons.remove),
                ),
                IconButton(
                  onPressed: () {
                    controlBlocProvider.addData();
                  },
                  icon: const Icon(Icons.add),
                ),
              ],
            ),
        ],
      )),
    );
  }
}
