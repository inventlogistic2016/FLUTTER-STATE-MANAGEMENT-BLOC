import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../controller/controller_bloc_provider.dart';

class counterDataBuilder extends StatelessWidget {
  const counterDataBuilder({super.key});

  @override
  Widget build(BuildContext context) {

    return BlocBuilder<ControlBlocProvider, int>(
        bloc: context.read<ControlBlocProvider>(),
        builder: (context, state) {
          return Text(
            "$state",
            style: const TextStyle(fontSize: 50),
          );
        });
  }
}

