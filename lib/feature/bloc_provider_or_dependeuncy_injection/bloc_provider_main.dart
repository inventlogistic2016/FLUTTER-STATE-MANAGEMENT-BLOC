import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'bloc_provider_page.dart';
import 'controller/controller_bloc_provider.dart';

class BlocProviderMain extends StatelessWidget {
  const BlocProviderMain({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ControlBlocProvider(),
      child: const MaterialApp(
        home: BlocProviderPage(),
      ),
    );
  }
}
