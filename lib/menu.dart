import 'package:flutter/material.dart';
import 'package:state_mangement_bloc/feature/bloc_builder/bloc_builder_page.dart';
import 'package:state_mangement_bloc/feature/bloc_consumer/bloc_consumer_page.dart';
import 'package:state_mangement_bloc/feature/bloc_listener/bloc_listener_page.dart';
import 'package:state_mangement_bloc/feature/cubit/cubit_page.dart';
import 'package:state_mangement_bloc/feature/menu_bloc/menu_bloc.dart';
import 'package:state_mangement_bloc/feature/menu_fetch_remote/menu_fetch_remote.dart';
import 'package:state_mangement_bloc/feature/menu_google_services/menu_google_services.dart';
import 'package:state_mangement_bloc/feature/menu_hardware/menu_hardware.dart';
import 'package:state_mangement_bloc/feature/observer_cubit/observer_cubit_page.dart';
import 'package:state_mangement_bloc/feature/stream/stream_page.dart';
import 'package:state_mangement_bloc/menu_item.dart';

class Menu extends StatelessWidget {
   Menu({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.grey[300],
          elevation: 0,
          toolbarHeight: 80,
          title: const Text(
            "",
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15),
        child: ListView(
          children: [
            MenuItem(text: "BLOC", pageBuilder: () => MenuBloc()),
            MenuItem(text: "FETCH REMOTE", pageBuilder: () => MenuFetchRemote()),
            MenuItem(text: "HARDWARE", pageBuilder: () => MenuHardware()),
            MenuItem(text: "GOOGLE SERVICES", pageBuilder: () => MenuGoogleServices()),
          ],
        ),
      ),
    );
  }
}
