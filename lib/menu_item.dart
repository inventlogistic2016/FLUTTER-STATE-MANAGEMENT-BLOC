import 'package:flutter/material.dart';

typedef PageBuilder = Widget Function();

class MenuItem extends StatelessWidget {
  final String text;

  final PageBuilder pageBuilder;

  const MenuItem({super.key, required this.text, required this.pageBuilder});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 30),
      child: ElevatedButton(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => pageBuilder()));
        },
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.grey[300],
          fixedSize: const Size(150, 50),
          shape: RoundedRectangleBorder(
            borderRadius:
                BorderRadius.circular(7.0), // Set your preferred radius
          ),
        ),
        child: Text(
          text,
          style: const TextStyle(color: Color(0xFF747D8C)),
        ),
      ),
    );
  }
}
